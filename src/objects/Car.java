package objects;

public abstract class Car {
    private String model;
    private int yearOfIssue;
    private int fuelConsumption;
    private int maxSpeed;
    private int volumeOfTheTank;
    private int acceleration;

    private int speed;

    public Car(String model, int yearOfIssue, int fuelConsumption, int maxSpeed, int volumeOfTheTank, int acceleration) {
        this.model = model;
        this.yearOfIssue = yearOfIssue;
        this.fuelConsumption = fuelConsumption;
        this.maxSpeed = maxSpeed;
        this.volumeOfTheTank = volumeOfTheTank;
        this.acceleration = acceleration;
    }

    public void Run() {
        if (speed + acceleration < maxSpeed)
            speed += acceleration;
        else
            speed = maxSpeed;
        if (volumeOfTheTank > fuelConsumption)
            volumeOfTheTank -= fuelConsumption;
        else {
            volumeOfTheTank = 0;
            Stop();
        }
        System.out.println(GetInfo());
    }

    private void Stop() {
        speed = 0;
    }

    public boolean IsStopped() {
        return speed == 0;
    }

    private String GetInfo() {
        String str = "";
        if (speed < 100)
            str = "Normal ";
        else if (speed < 120)
            str = "Average ";
        else str = "What the fuck are u doing ";
        return model + " Speed: " + str + speed + " Tank: " + volumeOfTheTank;
    }

    public String GetModel() {
        return model;
    }

    public void speak() {
        System.out.println("beep beep");
    }

    public static double ToMph(int kmh) {
        return kmh / 1.609;
    }

    public static double ToKmph(int mph) {
        return mph * 1.609;
    }
}
