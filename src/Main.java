import Inheritance.BMW;
import objects.Car;

public class Main {
    public static void main(String[] arqs) {
        Car car = new BMW("X5", 2015, 25, 200, 70, 70);
        System.out.println(car.GetModel());
        do {
            car.Run();
        }
        while (!car.IsStopped());


        System.out.println(Car.ToKmph(100));
        System.out.println(Car.ToMph(100));
    }
}

//model:"BMW", yearOfIssue:2015, fuelConsumption:15, maxSpeed:200, volumeOfTheTank:70