package Inheritance;
import objects.Car;

public class BMW extends Car{
    public BMW(String model, int yearOfIssue, int fuelConsumption, int maxSpeed, int volumeOfTheTank, int acceleration) {
        super(model, yearOfIssue, fuelConsumption, maxSpeed, volumeOfTheTank, acceleration);
    }
}
