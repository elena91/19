package Inheritance;
import objects.Car;

public class Audi extends Car {
    public Audi(String model, int yearOfIssue, int fuelConsumption, int maxSpeed, int volumeOfTheTank, int acceleration) {
        super(model, yearOfIssue, fuelConsumption, maxSpeed, volumeOfTheTank, acceleration);
    }
}
